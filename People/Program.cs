﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace People
{
    //    Задача 2
    //Да се дефинира клас, описващ човек.
    //Всеки човек има име и възраст.
    //Напишете програма, която чете N на брой хора от стандартния вход
    //(първи ред – име, втори ред – възраст) и извежда имената на онези от тях,
    //чиято възраст е между 18 и 64 години.
    //Задача 3
    //Добавете три конструктора към клaса от задача 2. Използвайте конструкторите, за да преизползвате кода:
    //•	Първият не приема параметри, създава човек с име „No name“ и години = 1
    //•	Вторият приема само цяло числo за възраст, създава човек с име „Бруно“ и години,
    //равни на стойността на параметъра
    //•	Третият трябва да приема като параметър името и годините на човека,
    //както и да създава инстанция на класа със съответните стойности

    public class Program
    {
        public static void Main(string[] args)
        {
            Person[] people = InputPeople();
            PrintPeopleFrom18To64(people);
        }

        public static Person[] InputPeople()
        {
            Console.Write("Enter people count: ");
            int count = int.Parse(Console.ReadLine());

            Person[] people = new Person[count];
            for (int i = 0; i < people.Length; i++)
            {
                //OPTION 1 - Assignment 2
                //Person person = new Person();
                //Console.WriteLine($"Enter information for Person {i + 1}");
                //Console.Write("Name = ");
                //person.Name = Console.ReadLine();

                //Console.Write("Age = ");
                //person.Age = int.Parse(Console.ReadLine());

                //people[i] = person;

                //OPTION 2 - Assignment 3
                Console.WriteLine($"Enter information for Person {i + 1}");
                Console.Write("Name = ");
                string name = Console.ReadLine();

                Console.Write("Age = ");
                int age = int.Parse(Console.ReadLine());

                people[i] = new Person(name, age);
            }

            return people;
        }

        public static void PrintPeopleFrom18To64(Person[] people)
        {
            for (int i = 0; i < people.Length; i++)
            {
                if (people[i].Age >= 18 && people[i].Age <= 64)
                {
                    Console.WriteLine(people[i].Name);
                }
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Points
{
    public class Program
    {
        public static void Main(string[] args)
        {
            Point p1 = new Point(3, 4);
            Point p2 = new Point(4, 5);

            Console.WriteLine(Point.CalculateDistance(p1, p2));

            Console.WriteLine(p1.CalculateDistance(p2));
        }
    }
}
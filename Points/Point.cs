﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Points
{
    public class Point
    {
        //properties
        public double X { get; set; }
        public double Y { get; set; }

        //constructor
        public Point(double x, double y)
        {
            X = x;
            Y = y;
        }

        //methods
        //static - calculate the distance between any two points
        public static double CalculateDistance(Point p1, Point p2)
        {
            return Math.Sqrt(Math.Pow(p1.X - p2.X, 2) + Math.Pow(p1.Y - p2.Y, 2));
        }

        //instance method - each instance of a point knows who to find the distance to another one
        public double CalculateDistance(Point point)
        {
            return Math.Sqrt(Math.Pow(this.X - point.X, 2) + Math.Pow(this.Y - point.Y, 2));
        }
    }
}
